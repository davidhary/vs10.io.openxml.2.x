﻿Imports System.Reflection
Imports System.Resources
' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

<Assembly: AssemblyTitle("ISR Open XML VB Library")> 
<Assembly: AssemblyDescription("ISR Open XML VB Library")> 
<Assembly: AssemblyCompany("Integrated Scientific Resources, Inc.")> 
<Assembly: AssemblyProduct("ISR Open XML VB Library 2.0")> 
<Assembly: AssemblyCopyright("(c) 2011 Integrated Scientific Resources, Inc.")> 
<Assembly: AssemblyTrademark("Licensed under the Apache 2.0 License")> 
<Assembly: NeutralResourcesLanguage("en-US", UltimateResourceFallbackLocation.MainAssembly)> 
<Assembly: AssemblyCulture("")> 
#If Not NoClsCompliant Then
<Assembly: CLSCompliant(True)> 
#End If

' Disable accessibility of an individual managed type or member, or of all types within an assembly, to COM.

<Assembly: System.Runtime.InteropServices.ComVisible(False)> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:

<Assembly: AssemblyVersion("2.0.*")> 
<Assembly: AssemblyFileVersionAttribute("2.0.0.0")> 
<Assembly: AssemblyInformationalVersion("2.0.0.0")> 

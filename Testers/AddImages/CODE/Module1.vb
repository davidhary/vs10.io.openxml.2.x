﻿Imports Microsoft.VisualBasic
Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.IO
Imports System.Drawing
Imports DocumentFormat.OpenXml
Imports DocumentFormat.OpenXml.Packaging
Imports DocumentFormat.OpenXml.Spreadsheet
Imports DocumentFormat.OpenXml.Drawing.Spreadsheet
Module Module1

    Sub Main(ByVal args As String())
        Dim sFile As String = "ExcelOpenXmlMultipleImageInsert.xlsx"
        If File.Exists(sFile) Then
            File.Delete(sFile)
        End If
        Try
            BuildWorkbook(sFile)
        Catch e As Exception
            Console.WriteLine(e.ToString())
        End Try
        Console.WriteLine("Program end")
    End Sub

    Sub BuildWorkbook(ByVal filename As String)
        Using xl As SpreadsheetDocument = SpreadsheetDocument.Create(filename, SpreadsheetDocumentType.Workbook)
            Dim wbp As WorkbookPart = xl.AddWorkbookPart()
            Dim wsp As WorksheetPart = wbp.AddNewPart(Of WorksheetPart)()
            Dim wb As Workbook = New Workbook()
            Dim fv As FileVersion = New FileVersion()
            fv.ApplicationName = "Microsoft Office Excel"
            Dim ws As Worksheet = New Worksheet()
            Dim sd As SheetData = New SheetData()

            Dim wbsp As WorkbookStylesPart = wbp.AddNewPart(Of WorkbookStylesPart)()
            wbsp.Stylesheet = CreateStylesheet()
            wbsp.Stylesheet.Save()

            Dim r As Row = New Row()
            r.RowIndex = 15
            Dim c As Cell
            c = New Cell()
            c.DataType = CellValues.String
            c.StyleIndex = 1
            c.CellReference = "G15"
            c.CellValue = New CellValue("We have multiple images!")
            r.Append(c)
            sd.Append(r)

            ws.Append(sd)
            wsp.Worksheet = ws

            ' It happens the resolution for the 3 images are 72 dpi
            ' Images are 240 by 180 pixels
            ' Adjust as needed
            isr.IO.OpenXml.Excel.InsertImage(ws, 0, 0, 2, 1.5, "..\..\resources\cloudstreaks.jpg")
            ' InsertImage(ws, 0, 0, "..\..\resources\cloudstreaks.jpg")
            ' 2286000 = 180 (pixels) * 914400 (magic number) / 72 (bitmap resolution)
            isr.IO.OpenXml.Excel.InsertImage(ws, 0.0, 2.5, "..\..\resources\crystallisedpavilion.png")
            ' InsertImage(ws, 0, 2286000, "..\..\resources\crystallisedpavilion.png")
            ' 3048000 = 240 (pixels) * 914400 (magic number) / 72 (bitmap resolution)
            ' InsertImage(ws, 3048000, 0, "..\..\resources\dozingcat.jpg")
            isr.IO.OpenXml.Excel.InsertImage(ws, 3.3333, 0.0, "..\..\resources\dozingcat.jpg")

            wsp.Worksheet.Save()
            Dim sheets As Sheets = New Sheets()
            Dim sheet As Sheet = New Sheet()
            sheet.Name = "Sheet1"
            sheet.SheetId = 1
            sheet.Id = wbp.GetIdOfPart(wsp)
            sheets.Append(sheet)
            wb.Append(fv)
            wb.Append(sheets)

            xl.WorkbookPart.Workbook = wb
            xl.WorkbookPart.Workbook.Save()
            xl.Close()
        End Using
    End Sub

    Function CreateStylesheet() As Stylesheet
        Dim ss As Stylesheet = New Stylesheet()

        Dim fts As Fonts = New Fonts()
        Dim ft As DocumentFormat.OpenXml.Spreadsheet.Font = New DocumentFormat.OpenXml.Spreadsheet.Font()
        Dim ftn As FontName = New FontName()
        ftn.Val = StringValue.FromString("Calibri")
        Dim ftsz As FontSize = New FontSize()
        ftsz.Val = DoubleValue.FromDouble(11)
        ft.FontName = ftn
        ft.FontSize = ftsz
        fts.Append(ft)

        ft = New DocumentFormat.OpenXml.Spreadsheet.Font()
        ftn = New FontName()
        ftn.Val = StringValue.FromString("Palatino Linotype")
        ftsz = New FontSize()
        ftsz.Val = DoubleValue.FromDouble(18)
        ft.FontName = ftn
        ft.FontSize = ftsz
        fts.Append(ft)

        fts.Count = UInt32Value.FromUInt32(CUInt(fts.ChildElements.Count))

        Dim fills As Fills = New Fills()
        Dim fill As Fill
        Dim patternFill As PatternFill
        fill = New Fill()
        patternFill = New PatternFill()
        patternFill.PatternType = PatternValues.None
        fill.PatternFill = patternFill
        fills.Append(fill)

        fill = New Fill()
        patternFill = New PatternFill()
        patternFill.PatternType = PatternValues.Gray125
        fill.PatternFill = patternFill
        fills.Append(fill)

        fill = New Fill()
        patternFill = New PatternFill()
        patternFill.PatternType = PatternValues.Solid
        patternFill.ForegroundColor = New ForegroundColor()
        patternFill.ForegroundColor.Rgb = HexBinaryValue.FromString("00ff9728")
        patternFill.BackgroundColor = New BackgroundColor()
        patternFill.BackgroundColor.Rgb = patternFill.ForegroundColor.Rgb
        fill.PatternFill = patternFill
        fills.Append(fill)

        fills.Count = UInt32Value.FromUInt32(CUInt(fills.ChildElements.Count))

        Dim borders As Borders = New Borders()
        Dim border As Border = New Border()
        border.LeftBorder = New LeftBorder()
        border.RightBorder = New RightBorder()
        border.TopBorder = New TopBorder()
        border.BottomBorder = New BottomBorder()
        border.DiagonalBorder = New DiagonalBorder()
        borders.Append(border)

        border = New Border()
        border.LeftBorder = New LeftBorder()
        border.LeftBorder.Style = BorderStyleValues.Thin
        border.RightBorder = New RightBorder()
        border.RightBorder.Style = BorderStyleValues.Thin
        border.TopBorder = New TopBorder()
        border.TopBorder.Style = BorderStyleValues.Thin
        border.BottomBorder = New BottomBorder()
        border.BottomBorder.Style = BorderStyleValues.Thin
        border.DiagonalBorder = New DiagonalBorder()
        borders.Append(border)
        borders.Count = UInt32Value.FromUInt32(CUInt(borders.ChildElements.Count))

        Dim csfs As CellStyleFormats = New CellStyleFormats()
        Dim cf As CellFormat = New CellFormat()
        cf.NumberFormatId = 0
        cf.FontId = 0
        cf.FillId = 0
        cf.BorderId = 0
        csfs.Append(cf)
        csfs.Count = UInt32Value.FromUInt32(CUInt(csfs.ChildElements.Count))

        Dim iExcelIndex As Integer = 164
        Dim nfs As NumberingFormats = New NumberingFormats()
        Dim cfs As CellFormats = New CellFormats()

        cf = New CellFormat()
        cf.NumberFormatId = 0
        cf.FontId = 0
        cf.FillId = 0
        cf.BorderId = 0
        cf.FormatId = 0
        cfs.Append(cf)

        Dim nfDateTime As NumberingFormat = New NumberingFormat()
        nfDateTime.NumberFormatId = CType(iExcelIndex, UInt32Value)
        iExcelIndex += 1
        nfDateTime.FormatCode = StringValue.FromString("dd/mm/yyyy hh:mm:ss")
        nfs.Append(nfDateTime)

        Dim nf4decimal As NumberingFormat = New NumberingFormat()
        nf4decimal.NumberFormatId = CType(iExcelIndex, UInt32Value)
        iExcelIndex += 1
        nf4decimal.FormatCode = StringValue.FromString("#,##0.0000")
        nfs.Append(nf4decimal)

        ' #,##0.00 is also Excel style index 4
        Dim nf2decimal As NumberingFormat = New NumberingFormat()
        nf2decimal.NumberFormatId = CType(iExcelIndex, UInt32Value)
        iExcelIndex += 1
        nf2decimal.FormatCode = StringValue.FromString("#,##0.00")
        nfs.Append(nf2decimal)

        ' @ is also Excel style index 49
        Dim nfForcedText As NumberingFormat = New NumberingFormat()
        nfForcedText.NumberFormatId = CType(iExcelIndex, UInt32Value)
        iExcelIndex += 1
        nfForcedText.FormatCode = StringValue.FromString("@")
        nfs.Append(nfForcedText)

        ' index 1
        cf = New CellFormat()
        cf.NumberFormatId = nfDateTime.NumberFormatId
        cf.FontId = 0
        cf.FillId = 0
        cf.BorderId = 0
        cf.FormatId = 0
        cf.ApplyNumberFormat = BooleanValue.FromBoolean(True)
        cfs.Append(cf)

        ' index 2
        cf = New CellFormat()
        cf.NumberFormatId = nf4decimal.NumberFormatId
        cf.FontId = 0
        cf.FillId = 0
        cf.BorderId = 0
        cf.FormatId = 0
        cf.ApplyNumberFormat = BooleanValue.FromBoolean(True)
        cfs.Append(cf)

        ' index 3
        cf = New CellFormat()
        cf.NumberFormatId = nf2decimal.NumberFormatId
        cf.FontId = 0
        cf.FillId = 0
        cf.BorderId = 0
        cf.FormatId = 0
        cf.ApplyNumberFormat = BooleanValue.FromBoolean(True)
        cfs.Append(cf)

        ' index 4
        cf = New CellFormat()
        cf.NumberFormatId = nfForcedText.NumberFormatId
        cf.FontId = 0
        cf.FillId = 0
        cf.BorderId = 0
        cf.FormatId = 0
        cf.ApplyNumberFormat = BooleanValue.FromBoolean(True)
        cfs.Append(cf)

        ' index 5
        ' Header text
        cf = New CellFormat()
        cf.NumberFormatId = nfForcedText.NumberFormatId
        cf.FontId = 1
        cf.FillId = 0
        cf.BorderId = 0
        cf.FormatId = 0
        cf.ApplyNumberFormat = BooleanValue.FromBoolean(True)
        cfs.Append(cf)

        ' index 6
        ' column text
        cf = New CellFormat()
        cf.NumberFormatId = nfForcedText.NumberFormatId
        cf.FontId = 0
        cf.FillId = 0
        cf.BorderId = 1
        cf.FormatId = 0
        cf.ApplyNumberFormat = BooleanValue.FromBoolean(True)
        cfs.Append(cf)

        ' index 7
        ' coloured 2 decimal text
        cf = New CellFormat()
        cf.NumberFormatId = nf2decimal.NumberFormatId
        cf.FontId = 0
        cf.FillId = 2
        cf.BorderId = 0
        cf.FormatId = 0
        cf.ApplyNumberFormat = BooleanValue.FromBoolean(True)
        cfs.Append(cf)

        ' index 8
        ' coloured column text
        cf = New CellFormat()
        cf.NumberFormatId = nfForcedText.NumberFormatId
        cf.FontId = 0
        cf.FillId = 2
        cf.BorderId = 1
        cf.FormatId = 0
        cf.ApplyNumberFormat = BooleanValue.FromBoolean(True)
        cfs.Append(cf)

        nfs.Count = UInt32Value.FromUInt32(CUInt(nfs.ChildElements.Count))
        cfs.Count = UInt32Value.FromUInt32(CUInt(cfs.ChildElements.Count))

        ss.Append(nfs)
        ss.Append(fts)
        ss.Append(fills)
        ss.Append(borders)
        ss.Append(csfs)
        ss.Append(cfs)

        Dim css As CellStyles = New CellStyles()
        Dim cs As CellStyle = New CellStyle()
        cs.Name = StringValue.FromString("Normal")
        cs.FormatId = 0
        cs.BuiltinId = 0
        css.Append(cs)
        css.Count = UInt32Value.FromUInt32(CUInt(css.ChildElements.Count))
        ss.Append(css)

        Dim dfs As DifferentialFormats = New DifferentialFormats()
        dfs.Count = 0
        ss.Append(dfs)

        Dim tss As TableStyles = New TableStyles()
        tss.Count = 0
        tss.DefaultTableStyle = StringValue.FromString("TableStyleMedium9")
        tss.DefaultPivotStyle = StringValue.FromString("PivotStyleLight16")
        ss.Append(tss)

        Return ss
    End Function

    Sub InsertImage(ByVal ws As Worksheet, ByVal x As Long, ByVal y As Long, ByVal width As Nullable(Of Long), ByVal height As Nullable(Of Long), ByVal sImagePath As String)
        Try
            Dim wsp As WorksheetPart = ws.WorksheetPart
            Dim dp As DrawingsPart
            Dim imgp As ImagePart
            Dim wsd As WorksheetDrawing

            Dim ipt As ImagePartType
            Select Case sImagePath.Substring(sImagePath.LastIndexOf("."c) + 1).ToLower()
                Case "png"
                    ipt = ImagePartType.Png
                Case "jpg", "jpeg"
                    ipt = ImagePartType.Jpeg
                Case "gif"
                    ipt = ImagePartType.Gif
                Case Else
                    Return
            End Select

            If wsp.DrawingsPart Is Nothing Then
                '----- no drawing part exists, add a new one

                dp = wsp.AddNewPart(Of DrawingsPart)()
                imgp = dp.AddImagePart(ipt, wsp.GetIdOfPart(dp))
                wsd = New WorksheetDrawing()
            Else
                '----- use existing drawing part

                dp = wsp.DrawingsPart
                imgp = dp.AddImagePart(ipt)
                dp.CreateRelationshipToPart(imgp)
                wsd = dp.WorksheetDrawing
            End If

            Using fs As FileStream = New FileStream(sImagePath, FileMode.Open)
                imgp.FeedData(fs)
            End Using

            Dim imageNumber As Integer = dp.ImageParts.Count() '  dp.ImageParts.Count(Of ImagePart)()
            If imageNumber = 1 Then
                Dim drawing As Drawing = New Drawing()
                drawing.Id = dp.GetIdOfPart(imgp)
                ws.Append(drawing)
            End If

            Dim nvdp As NonVisualDrawingProperties = New NonVisualDrawingProperties()
            nvdp.Id = New UInt32Value(CUInt(1024 + imageNumber))
            nvdp.Name = "Picture " & imageNumber.ToString()
            nvdp.Description = ""
            Dim picLocks As DocumentFormat.OpenXml.Drawing.PictureLocks = New DocumentFormat.OpenXml.Drawing.PictureLocks()
            picLocks.NoChangeAspect = True
            picLocks.NoChangeArrowheads = True
            Dim nvpdp As NonVisualPictureDrawingProperties = New NonVisualPictureDrawingProperties()
            nvpdp.PictureLocks = picLocks
            Dim nvpp As NonVisualPictureProperties = New NonVisualPictureProperties()
            nvpp.NonVisualDrawingProperties = nvdp
            nvpp.NonVisualPictureDrawingProperties = nvpdp

            Dim stretch As DocumentFormat.OpenXml.Drawing.Stretch = New DocumentFormat.OpenXml.Drawing.Stretch()
            stretch.FillRectangle = New DocumentFormat.OpenXml.Drawing.FillRectangle()

            Dim blipFill As BlipFill = New BlipFill()
            Dim blip As DocumentFormat.OpenXml.Drawing.Blip = New DocumentFormat.OpenXml.Drawing.Blip()
            blip.Embed = dp.GetIdOfPart(imgp)
            blip.CompressionState = DocumentFormat.OpenXml.Drawing.BlipCompressionValues.Print
            blipFill.Blip = blip
            blipFill.SourceRectangle = New DocumentFormat.OpenXml.Drawing.SourceRectangle()
            blipFill.Append(stretch)

            Dim t2d As DocumentFormat.OpenXml.Drawing.Transform2D = New DocumentFormat.OpenXml.Drawing.Transform2D()
            Dim offset As DocumentFormat.OpenXml.Drawing.Offset = New DocumentFormat.OpenXml.Drawing.Offset()
            offset.X = 0
            offset.Y = 0
            t2d.Offset = offset
            Dim bm As Bitmap = New Bitmap(sImagePath)

            Dim extents As DocumentFormat.OpenXml.Drawing.Extents = New DocumentFormat.OpenXml.Drawing.Extents()

            If Not width.HasValue Then
                extents.Cx = CLng(Fix(bm.Width)) * CLng(Fix(CSng(914400) / bm.HorizontalResolution))
            Else
                extents.Cx = CType(width, Int64Value)
            End If

            If Not height.HasValue Then
                extents.Cy = CLng(Fix(bm.Height)) * CLng(Fix(CSng(914400) / bm.VerticalResolution))
            Else
                extents.Cy = CType(height, Int64Value)
            End If

            bm.Dispose()
            t2d.Extents = extents
            Dim sp As ShapeProperties = New ShapeProperties()
            sp.BlackWhiteMode = DocumentFormat.OpenXml.Drawing.BlackWhiteModeValues.Auto
            sp.Transform2D = t2d
            Dim prstGeom As DocumentFormat.OpenXml.Drawing.PresetGeometry = New DocumentFormat.OpenXml.Drawing.PresetGeometry()
            prstGeom.Preset = DocumentFormat.OpenXml.Drawing.ShapeTypeValues.Rectangle
            prstGeom.AdjustValueList = New DocumentFormat.OpenXml.Drawing.AdjustValueList()
            sp.Append(prstGeom)
            sp.Append(New DocumentFormat.OpenXml.Drawing.NoFill())

            Dim picture As DocumentFormat.OpenXml.Drawing.Spreadsheet.Picture = New DocumentFormat.OpenXml.Drawing.Spreadsheet.Picture()
            picture.NonVisualPictureProperties = nvpp
            picture.BlipFill = blipFill
            picture.ShapeProperties = sp

            Dim pos As Position = New Position()
            pos.X = x
            pos.Y = y
            Dim ext As Extent = New Extent()
            ext.Cx = extents.Cx
            ext.Cy = extents.Cy
            Dim anchor As AbsoluteAnchor = New AbsoluteAnchor()
            anchor.Position = pos
            anchor.Extent = ext
            anchor.Append(picture)
            anchor.Append(New ClientData())
            wsd.Append(anchor)
            wsd.Save(dp)
        Catch ex As Exception
            Throw ex ' or do something more interesting if you want
        End Try
    End Sub

    Sub InsertImage(ByVal ws As Worksheet, ByVal x As Long, ByVal y As Long, ByVal sImagePath As String)
        InsertImage(ws, x, y, Nothing, Nothing, sImagePath)
    End Sub

End Module
